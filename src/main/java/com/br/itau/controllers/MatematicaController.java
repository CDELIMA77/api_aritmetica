package com.br.itau.controllers;

import com.br.itau.models.Matematica;
import com.br.itau.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")

public class MatematicaController {
        @Autowired
        private MatematicaService matematicaService;

        @PostMapping("soma")
        public Integer soma (@RequestBody Matematica matematica) {
            List<Integer> numeros = matematica.getNumeros();

            if (numeros.isEmpty() || numeros.size() < 2) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "E necessario ao menos dois numeros para serem somados");
            }
            return matematicaService.soma(numeros);
        }

        @PostMapping("multiplica")
        public Integer multiplica (@RequestBody Matematica matematica) {
            List<Integer> numeros = matematica.getNumeros();
            if (numeros.isEmpty() || numeros.size() != 2) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Somente informar dois numeros para serem multiplicados");
            }
            return matematicaService.multiplica(numeros);
        }

    @PostMapping("subtrai")
    public Integer subtrai (@RequestBody Matematica matematica) {
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Somente informar dois numeros para serem subtraidos");
        }
               return matematicaService.subtrai(numeros);

        }

    @PostMapping("divide")
    public Double divide (@RequestBody Matematica matematica) {
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Somente informar dois numeros para serem divididos");
        }
        return matematicaService.divide(numeros);
    }


}
