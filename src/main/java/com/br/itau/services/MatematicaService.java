package com.br.itau.services;

import com.br.itau.models.Matematica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public class MatematicaService {

        public int soma(List<Integer> numeros) {

            int resultado = 0;
            for (int numero : numeros) {
                resultado += numero;
            }
            return resultado;
        }

        public int multiplica(List<Integer> numeros) {

                int resultadoM = 1;
                for (int numero : numeros) {
                    resultadoM *= numero;
                }
                return resultadoM;
        }

        public int subtrai(List<Integer> numeros) {

            int resultadoS = 0;
            for (int numero : numeros) {
                if (numeros.indexOf(numero) == 0 ) {
                resultadoS = numero;
                } else {
                resultadoS -= numero;
               }
            }
            return resultadoS;
        }

    public Double divide(List<Integer> numeros) {

        double resultadoD = 0;
        if (numeros.get(0) >= numeros.get(1)) {
            resultadoD = numeros.get(0) / numeros.get(1);
        } else {
            resultadoD = numeros.get(1) / numeros.get(0);
        }
        return resultadoD;
    }

}